<body>
<?php 
require 'header.php';
require 'mdp.php';
$dbh = new PDO('mysql:host=localhost;dbname=php_bdd',$utilisateur, $password);
$resultats=$dbh->query("SELECT * FROM utilisateurs");
?>

<h1>Super titre original en plus de ça</h1>

<table>
<thead>
    <tr>
        <th>id</th>
        <th>nom</th>
        <th>prenom</th>
        <th>numéro</th>
        <th>rue</th>
        <th>code postal</th>
        <th>ville</th>
        <th>email</th>
    </tr>    
</thead>
<tbody>
    
    <?php foreach($resultats as $key=> $resultat): ?>
    <tr>
        <td><?php echo $resultat['id']; ?></td> 
        <td><?php echo $resultat['nom']; ?></td>
        <td><?php echo $resultat['prenom']; ?></td>
        <td><?php echo $resultat['num_rue']; ?></td>
        <td><?php echo $resultat['nom_rue']; ?></td>
        <td><?php echo $resultat['cp']; ?></td>
        <td><?php echo $resultat['ville']; ?></td>
        <td><?php echo $resultat['email']; ?></td>
    </tr>
    <?php endforeach; ?>

    
</tbody>        
</table>   
   

</body>
<?php require 'footer.php'; ?>
